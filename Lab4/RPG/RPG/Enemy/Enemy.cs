﻿public class Enemy
{

	public string Name { get; set; }
	public int Hp { get; set; }
	public int Damage { get; set; }
	public string Image { get; set; }

}