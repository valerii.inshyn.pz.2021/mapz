﻿using RPG.Hero.Weapons;

namespace RPG.Hero
{
    public sealed class Singleton
    {
        private Singleton() { }

        private static Singleton? _instance;

        public bool isActive = false;
        public double Money { get; set; }
        public IWeapon Weapon = new Hand();

        public int HP { get; set; } = 5;
        public int Brutality { get; set; } = 1;
        public int Agility { get; set; } = 1;


        public static Singleton GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Singleton();
            }
            return _instance;
        }
    }
}
