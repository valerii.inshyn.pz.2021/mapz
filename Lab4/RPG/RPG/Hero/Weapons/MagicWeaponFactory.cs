﻿namespace RPG.Hero.Weapons
{
    public class MagicWeaponFactory : WeaponFactory
    {
        public Sword CreateSword()
        {
            return new MagicSword() { Damage = 7, Price = 10, ImageRoute = "magicsword.png" };
        }
    }
}
