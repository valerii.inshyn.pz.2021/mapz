﻿namespace RPG.Hero.Weapons
{
    public interface WeaponFactory
    {
        public Sword CreateSword();
    }
}
