﻿namespace RPG.Hero.Weapons
{
    public interface IWeapon
    {
        public int Damage { get; init; }
        public double Price { get; init; }
        public string ImageRoute { get; init; }
    }
}
