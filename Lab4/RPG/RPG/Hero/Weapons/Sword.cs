﻿namespace RPG.Hero.Weapons
{
    public abstract class Sword : IWeapon
    {
        public int Damage { get; init; }
        public double Price { get; init; }
        public string ImageRoute { get; init; }
    }
}
