﻿namespace RPG.Hero.Weapons
{
    public class PhysicalWeaponFactory : WeaponFactory
    {
        public Sword CreateSword()
        {
            return new PhysicalSword() { Damage = 5, Price = 10, ImageRoute = "physicalsword.png" };
        }
    }
}
