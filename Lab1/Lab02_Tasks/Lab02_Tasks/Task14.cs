﻿namespace Lab02_Tasks
{
    public class Task14 : object
    {

        public string Message { get; }

        public Task14(string message)
        {
            Message = message;
        }

        public override string ToString()
        {
            return Message;
        }

        public override bool Equals(object? obj)
        {
            return obj is Task14 account && account.Message == Message;
        }

        public override int GetHashCode()
        {
            return Message.GetHashCode();
        }
    }

}
