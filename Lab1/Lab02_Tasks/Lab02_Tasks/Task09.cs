﻿namespace Lab02_Tasks
{
    public static class Task09
    {
        // Refs

        public static void Swap<T>(ref T a, ref T b)
        {
            (a,b) = (b,a);
        }

        // Out

        public static void SumInts(int a, int b,out int result)
        {
            result =  a + b;
        }

        // No ref

        public static (T a,T b) Swap<T>(T a, T b)
        {
            return (b, a);
        }

        // No out

        public static int SumInts(int a, int b)
        {
            return a + b;
        }

        // No matter refs or out
        public static void ReturnConstantValueRef(ref int value)
        {
            value = 1;
        }

        public static void ReturnConstantValueOut(out int value)
        {
            value = 1;
        }


    }
}
