﻿namespace Lab02_Tasks
{
    public interface ITask06_First
    {
        string Run();
    }

    public interface ITask06_Second
    {
        string Sleep();
    }

    public interface ITask06_Third
    {
        string Prop { get; }
    }

    public abstract class ITask06_Abstract : ITask06_Third
    {
        public string Prop { get; }

        protected ITask06_Abstract(string prop)
        {
            Prop = prop;
        }
    }

    public class DerivedClass : ITask06_Abstract, ITask06_First, ITask06_Second
    {
        public DerivedClass(string prop) : base(prop)
        {
        }

        public string Run()
        {
            throw new NotImplementedException();
        }

        public string Sleep()
        {
            throw new NotImplementedException();
        }
    }


}

