﻿
using Lab02_Tasks;

var AccessExample = new AccessExampleBase();


// Task 05

var none = Task05Enum.None;
var first = Task05Enum.First;
var second = Task05Enum.Second;
var third = Task05Enum.Third;
var all = Task05Enum.All;

//Console.WriteLine(first && second); 
//Console.WriteLine(first ||  second); 
Console.WriteLine(first & second);
Console.WriteLine(first | third);
Console.WriteLine(second ^ all);
Console.WriteLine(~third);


Console.WriteLine("\n========================\n");

// Task 08

var task08 = new Task08();
Console.WriteLine($"Static field: {Task08.StaticField}");
Console.WriteLine($"Instance field: {task08.InstanceField}");

Console.WriteLine("\n========================\n");


// Task 09

int x = 100, y = 50;
int res = 0;

// Ref
Task09.Swap(x, y);

// No Ref
(x, y) = Task09.Swap(x, y);

// Out
Task09.SumInts(x, y, out res);
res = Task09.SumInts(x, y);

// No matter
Task09.ReturnConstantValueRef(ref res);

Task09.ReturnConstantValueOut(out res);


// Task 10

// Create variable
int value = 32453354;
Console.WriteLine(value);
object obj = value; // Boxing
Console.WriteLine(obj);
int newValue = (int)obj; // Unboxing
Console.WriteLine(newValue);
