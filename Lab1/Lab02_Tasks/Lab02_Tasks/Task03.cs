﻿namespace Lab02_Tasks
{
    class Task03 // internal
    {
        int _field; // private
        int Prop { get; set; } // public
        interface IInnerInterface // public
        {

        }
        abstract class InnerAbstractClass // private
        {

        }
        class InnerClass // private
        {

        }
        struct InnerStruct // private
        {

        }
        void InnerMethod() // private
        {

        }
    }
    interface IInterface // internal
    {
        int Prop { get; set; } // public
        void InnerMethod() // public
        {

        }
    }
    abstract class AbstractClass // internal
    {
        int _field; // private
        int Prop { get; set; } // private
        void InnerMethod() // private
        {

        }
    }
    struct Struct // internal
    {
        int _field; // private
        int Prop { get; set; } // private
        void InnerMethod() // private
        {

        }
    }

}
