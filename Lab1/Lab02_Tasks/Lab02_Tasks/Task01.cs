﻿
namespace Lab02_Tasks
{
    public interface IAnimal
    {
        void Talk();
        void SayMyName();
    }

    public abstract class Bird : IAnimal
    {
        public virtual void Talk()
        {
            Console.WriteLine("Hello world!"); 
        }

        public virtual void SayMyName()
        {
            Console.WriteLine("I'm a bird");
        }
    }

    public class Eagle : Bird
    {
        public override void SayMyName()
        {
            Console.WriteLine($"I'm a {this.GetType().Name}");
        }
    }

}
