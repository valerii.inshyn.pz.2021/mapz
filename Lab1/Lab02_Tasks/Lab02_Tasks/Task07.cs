﻿namespace Lab02_Tasks
{
    public abstract class Group
    {
        public string Title { get; }
        public int Size { get; }

        protected Group(string title) : this(title, 0)
        {
        }

        protected Group(string title, int size)
        {
            Title = title;
            Size = size;
        }

        public virtual void GetGroupTitle()
        {
            Console.WriteLine($"Group title: {Title}");
        }
    }

    public class Team : Group
    {
        public string SportType { get; }

        public Team(string title, string sportType) : this(title, 0, sportType)
        {
        }

        public Team(string title, int size, string sportType) : base(title, size)
        {
            SportType = sportType;
        }

        public override void GetGroupTitle()
        {
            Console.WriteLine($"{SportType} : Group title : {Title}");
        }
    }

}
