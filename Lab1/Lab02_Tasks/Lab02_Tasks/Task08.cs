﻿namespace Lab02_Tasks
{
    class Task08
    {
        private static int _staticField = GetStaticFieldValue("private - out of constructor");
        private int _instanceField = GetInstanceFieldValue("private - out of constructor");

        public static int StaticField = GetStaticFieldValue("public - out of constructor");
        public int InstanceField = GetInstanceFieldValue("public - out of constructor");

        static Task08()
        {
            Console.WriteLine("Static constructor called.");
            _staticField = GetStaticFieldValue("private - in constructor");
            StaticField = GetStaticFieldValue("public - in constructor");
        }

        public Task08()
        {
            Console.WriteLine("Instance constructor called.");
            _instanceField = GetInstanceFieldValue("private - in constructor");
            InstanceField = GetInstanceFieldValue("public - in constructor");
        }

        private static int GetInstanceFieldValue(string context)
        {
            Console.WriteLine($"GetInstanceFieldValue method called. [{context}]");
            return 1;
        }

        private static int GetStaticFieldValue(string context)
        {
            Console.WriteLine($"GetStaticFieldValue method called. [{context}]");
            return 1;
        }
    }

}
