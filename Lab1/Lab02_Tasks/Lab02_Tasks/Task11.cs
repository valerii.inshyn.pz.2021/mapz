﻿namespace Lab02_Tasks
{
    public class Task11
    {
        private double Value { get; }

        public Task11(double value)
        {
           
            Value = value;
        }

        public static implicit operator double(Task11 d) => d.Value;
        public static explicit operator Task11(double b) => new(b);

        public override string ToString() => Value.ToString();
    }

}
