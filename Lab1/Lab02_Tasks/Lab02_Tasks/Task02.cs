﻿namespace Lab02_Tasks
{
    public class AccessExampleBase
    {
        public int PublicProp;
        private int privateProp;
        internal int InternalProp;
        protected int ProtectedProp;
        protected internal int ProtectedInternalProp;

        public AccessExampleBase()
        {
           
        }

        public int GetPublic()
        {
            return PublicProp;
        }
        private int GetPrivate()
        {
            return privateProp;
        }
        internal int GetInternal()
        {
            return InternalProp;
        }
        protected int GetProtected()
        {
            return ProtectedProp;
        }
        protected internal int GetProtectedInternal()
        {
            return ProtectedInternalProp;
        }

    }


    public class AccessExampleDerived : AccessExampleBase 
    {
        public AccessExampleDerived()
        {
            
        }
    }
}
